package com.mindtree.services.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.mindtree.exceptions.ServiceException;
import com.mindtree.services.DbConfigurationService;

public class DbConfigurationServiceImpl implements DbConfigurationService {

	Connection con = null;

	@Override
	public boolean loadDriver() throws ServiceException {
		try {
			Class.forName(mySQLdriver);
			return true;
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			throw new ServiceException("Exception in loading driver", e);
		}
	}

	@Override
	public Connection getConnection() throws ServiceException {
		try {
			if (con == null) {
				con = DriverManager.getConnection(DB_URL, USER, PASS);
				return con;
			}
			return null;
		} catch (SQLException e) {
			throw new ServiceException("Exception in getting connection", e);
		}
	}

	@Override
	public boolean closeConnection() throws ServiceException {

		try {
//			if (!con.isClosed()) {
				con.close();
				return true;
//			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			throw new ServiceException("Exception in closing the connection", e);
		}
//		return false;

	}

}
