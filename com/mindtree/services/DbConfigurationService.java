package com.mindtree.services;

import java.sql.Connection;

import com.mindtree.exceptions.ServiceException;


public interface DbConfigurationService {
	final String mySQLdriver="com.mysql.jdbc.Driver";
	final String DB_URL = "jdbc:mysql://localhost:3306/mindtree";
	//  Database credentials
    final String USER = "root";
    final String PASS = "login@123";
	public boolean loadDriver() throws ServiceException;
	public Connection getConnection() throws ServiceException;
	public boolean closeConnection() throws ServiceException;
	
	}
