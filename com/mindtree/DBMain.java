package com.mindtree;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mindtree.exceptions.ServiceException;
import com.mindtree.services.DbConfigurationService;
import com.mindtree.services.impl.DbConfigurationServiceImpl;

public class DBMain {

	public static void main(String[] args) {
		DbConfigurationService dbService = new DbConfigurationServiceImpl();
		Connection con = null;
		System.out.println("program started");
		try {
			if (dbService.loadDriver()) {
				System.out.println("driver loaded successfully");
				con = dbService.getConnection();
				if (con != null) {
					PreparedStatement preparedStatement = con.prepareStatement("select * from employee");
					ResultSet resultSet = preparedStatement.executeQuery();

					while (resultSet.next()) {
						System.out.println("id : " + resultSet.getInt(1));
						System.out.println("name : " + resultSet.getString(2));
						System.out.println("salary : " + resultSet.getString(3));
					}
					dbService.closeConnection();
					if (dbService.closeConnection())
						System.out.println("db connection closed successfully");
				}
				else
					System.out.println("connection null");
			} else {
				System.out.println("unable to load driver");
			}

		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
